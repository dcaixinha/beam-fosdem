---
title: Call for Talks
subtitle: Come and share your experiences with us!
date: "2019-10-08"
comments: false
---

{{% center %}}
⚠️ _The Call for Talks has officially ended. You can find the final schedule [here](/schedule)._ ⚠️
{{% /center %}}

The Erlang, Elixir & Friends Devroom will take place on **Saturday, 1 February 2020**. We invite authors to submit original, high-quality work with sufficient background material to be clear to the BEAM community.

We welcome any talk proposals about BEAM-related projects. Topics of interest include, but are not limited to:

  - Languages running on the BEAM (Erlang, Elixir, *et. al.*)
    - Introduction to the language
    - Recent features and roadmap
    - Interesting libraries
  - Frameworks
  - Open Source projects that run on the BEAM
  - Tooling
  - Scalability and reliability
  - Community initiatives

Talks slots are aimed at 20 minutes each (eventually including time for a short Q&A).

Please be aware of the fact that Devroom talks at FOSDEM will be recorded. By submitting a proposal you agree to being recorded and to have your talk made available.

Submissions must include:

  - Title (Event title in *Pentabarf*)
  - Abstract

These additional details must be included in the *'Submission notes'* section:

  - Expected prior knowledge / intended audience
  - Speaker bio
  - Links to previous talks by the speaker (optional)
  - Links to code / slides / material for the talk (optional)

If you already have a *Pentabarf* account from a previous FOSDEM edition, please reuse it. Create an account if, and only if, you don’t have one from a previous year.

When logged in, click on the *'Create Event'* button (on the left menu) to create your talk proposal. Before submitting your talk in *Pentabarf*, please make sure that **Erlang, Elixir & Friends** Devroom is selected as the **Track**, and also that the duration is 20 minutes.

If you have any issues with *Pentabarf*, do not despair! Reach us at *beam-devroom[at]lists.fosdem.org*.

<p style="text-align: center">
  <i class="fab"><strong>Click here to submit your talk!</strong></i>
</p>

## Important Dates

 - Call for papers opens: **14 October 2019**
 - Call for papers closes: **25 November 2019**
 - Devroom schedule available: **16 December 2019**
 - Devroom day: **Saturday, 1 February 2020 (10:30–14:30)**

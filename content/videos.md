---
title: Talk videos
subtitle: Video recordings from the FOSDEM BEAM devroom
date: "2020-02-03"
comments: false
---

## FOSDEM 2020

------

### Farwest Demo: A website/API for a document oriented database in 20 minutes

by Loïc Hoguin

<video controls>
  <source src="https://video.fosdem.org/2020/AW1.121/beam_farwest_demo.mp4" type="video/mp4" />
  <source src="https://video.fosdem.org/2020/AW1.121/beam_farwest_demo.webm" type="video/webm" />
</video>

[FOSDEM talk page](https://fosdem.org/2020/schedule/event/beam_farwest_demo/)

------

### OpenTelemetry: an XKCD 927 Success Story

by Greg Mefford

<video controls>
  <source src="https://video.fosdem.org/2020/AW1.121/beam_opentelemetry_xkcd_927_success_story.mp4" type="video/mp4" />
  <source src="https://video.fosdem.org/2020/AW1.121/beam_opentelemetry_xkcd_927_success_story.webm" type="video/webm" />
</video>

[FOSDEM talk page](https://fosdem.org/2020/schedule/event/beam_opentelemetry_xkcd_927_success_story/)

------

### Debugging and tracing a production RabbitMQ node

by Gabriele Santomaggio

<video controls>
  <source src="https://video.fosdem.org/2020/AW1.121/beam_debugging_tracing_rabbitmq_node.mp4" type="video/mp4" />
  <source src="https://video.fosdem.org/2020/AW1.121/beam_debugging_tracing_rabbitmq_node.webm" type="video/webm" />
</video>

[FOSDEM talk page](https://fosdem.org/2020/schedule/event/beam_debugging_tracing_rabbitmq_node/)

------

### Keep Calm and Use Nerves

by Arjan Scherpenisse

<video controls>
  <source src="https://video.fosdem.org/2020/AW1.121/beam_keep_calm_use_nerves.mp4" type="video/mp4" />
  <source src="https://video.fosdem.org/2020/AW1.121/beam_keep_calm_use_nerves.webm" type="video/webm" />
</video>

[FOSDEM talk page](https://fosdem.org/2020/schedule/event/beam_keep_calm_use_nerves/)

------

### Lumen: Elixir in the Browser

by Luke Imhoff

<video controls>
  <source src="https://video.fosdem.org/2020/AW1.121/beam_lumen_elixir_browser.mp4" type="video/mp4" />
  <source src="https://video.fosdem.org/2020/AW1.121/beam_lumen_elixir_browser.webm" type="video/webm" />
</video>

[FOSDEM talk page](https://fosdem.org/2020/schedule/event/beam_lumen_elixir_browser/)

------

### CoffeeBeam: A BEAM VM for Android

by Viktor Gergely

<video controls>
  <source src="https://video.fosdem.org/2020/AW1.121/beam_coffeebeam_beam_vm_android.mp4" type="video/mp4" />
  <source src="https://video.fosdem.org/2020/AW1.121/beam_coffeebeam_beam_vm_android.webm" type="video/webm" />
</video>

[FOSDEM talk page](https://fosdem.org/2020/schedule/event/beam_coffeebeam_beam_vm_android/)


------

### Going Meta with Elixir's Macros

by Wiebe-Marten Wijnja

<video controls>
  <source src="https://video.fosdem.org/2020/AW1.121/beam_going_meta_elixir_macros.mp4" type="video/mp4" />
  <source src="https://video.fosdem.org/2020/AW1.121/beam_going_meta_elixir_macros.webm" type="video/webm" />
</video>

[FOSDEM talk page](https://fosdem.org/2020/schedule/event/beam_going_meta_elixir_macros/) (doesn't contain the above videos yet)

------

### Processes & Grains: A Journey in Orleans

by Evadne Wu

<video controls>
  <source src="https://video.fosdem.org/2020/AW1.121/beam_processes_grains_journey_orleans.mp4" type="video/mp4" />
  <source src="https://video.fosdem.org/2020/AW1.121/beam_processes_grains_journey_orleans.webm" type="video/webm" />
</video>

[FOSDEM talk page](https://fosdem.org/2020/schedule/event/beam_processes_grains_journey_orleans/)
